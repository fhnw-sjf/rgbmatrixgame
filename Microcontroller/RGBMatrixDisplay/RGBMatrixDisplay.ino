#include <ArduinoJson.h>
#include <Adafruit_NeoPixel.h>

#define syncTime 10
#define PIN         2
#define NUMPIXELS   64

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

unsigned long delayMillis = 10;
unsigned long lastMillis = 0;
unsigned long lastSyncMillis = 0;


void setup()
{
  Serial.begin(9600);
  Serial.flush();
  // initialize LED strip
  pixels.begin();
  pixels.clear();
  pixels.show();
}

void loop()
{

  long currentMillis = millis();

  if (currentMillis - lastMillis > delayMillis)
  {
    lastMillis = currentMillis;
    download();
  }
}


void download()
{
  while (!Serial.available() > 0)
  {
    long currentMillis = millis();

    if (currentMillis - lastSyncMillis > syncTime)
    {
      lastSyncMillis = currentMillis;
      break;
    }
  }

  DynamicJsonDocument doc(1024);
  DeserializationError err = deserializeJson(doc, Serial);

  if (doc["t"] == 0){
      JsonArray array = doc["c"].as<JsonArray>();
      int idx = 0;
      int r = 0;
      int g = 0;
      int b = 0;
      int ct = 0;
      for(JsonVariant v : array) {
          if (ct == 0){
            Serial.println(idx);
            idx = v.as<int>();
          }
          if (ct == 1){
            r = v.as<int>();
          }
          if (ct == 2){
            g = v.as<int>();
          }
          if (ct == 3){
            b = v.as<int>();
            pixels.setPixelColor(idx, pixels.Color(r, g, b));
            ct = -1;
          }
          ct++;
      }
      if (doc["s"]){
        pixels.show();
      }
  }else if (doc["t"] == 1){
    pixels.setBrightness(doc["v"]);
    pixels.show();
  }else if (doc["t"] == 2){
    pixels.clear();
    pixels.show();
  }
  
  if (err){
    Serial.flush();
    return;
  }
  doc.clear();

}
