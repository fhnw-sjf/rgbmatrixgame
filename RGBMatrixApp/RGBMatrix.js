const C = {
    BLUE:   [0,     0,      255],
    RED:    [255,   0,      0],
    GREEN:  [48,    122,    10],
    YELLOW: [255,   255,    0],
    GRAY:   [50,    50,     50],
    WHITE:  [255,   255,    255],
    BLACK:  [0,     0,      0]
}

class RGBMatrix {
    static getColors() {
        return C;
    }
    constructor(microcontroller) {
        this.microcontroller = microcontroller;

        this.displayMatrix = this.displayMatrix.bind(this);
        this.setBrightness = this.setBrightness.bind(this);
        this.clearDisplay  = this.clearDisplay.bind(this);
        this.wait          = this.wait.bind(this);
        this._flatten      = this._flatten.bind(this);
    }

    async displayMatrix(matrix){
        const flattMatrix = this._flatten(matrix);
        for (let i = 0; i < 4; i++){
            await this.microcontroller.sendCommand({"c": flattMatrix.slice(i * 64, (i + 1) * 64), "s": i === 3, "t": 0});
        }
    }

    async setBrightness(brightness){
        await this.microcontroller.sendCommand({"t": 1, "v": brightness});
    }

    async clearDisplay(){
        await this.microcontroller.sendCommand({"t": 2});
    }

    async wait(period){
        return await new Promise(resolve => setTimeout(() => resolve(), period));
    }

    _flatten(arr){
        const flat = [];
        let idx = 0;
        for (let i = 0; i < arr.length; i++){
            for (let s = 0; s < arr[i].length; s++){
                if (Array.isArray(arr[i][s])){
                    flat.push(idx);
                    for (let k = 0; k < arr[i][s].length; k++){
                        flat.push(arr[i][s][k]);
                    }
                    ++idx;
                }
            }
        }
        return flat;
    }
}

module.exports = RGBMatrix;
