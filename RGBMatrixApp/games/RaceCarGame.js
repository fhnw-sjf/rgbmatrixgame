const RGBMatrix = require('../RGBMatrix.js');
const Keyboard  = require('../Keyboard.js');

class Map {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.map = [];
        this.crashed = false;
        this.c = RGBMatrix.getColors();
        this.roadWidth = 3;
        this.boundaries = [
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5},
            {left: 2, right: 5}
        ];
        this.carPosition = {x: 4, y: 6}

        for (let s = 0; s < this.height; s++){
            this.map.push([]);
            for (let t = 0; t < this.width; t++){
                this.map[s].push([this.c.BLACK]);
            }
        }

        this.changeRoad        = this.changeRoad.bind(this);
        this.drawRoad          = this.drawRoad.bind(this);
        this.updateCarPosition = this.updateCarPosition.bind(this);
        this.isCrashed         = this.isCrashed.bind(this);
    }

    isCrashed(){
        return this.crashed;
    }

    changeRoad(){
        const bernoulli1 = Math.floor(Math.random() * 2);
        const bernoulli2 = Math.floor(Math.random() * 2);
        if (bernoulli2 === 1){
            this.roadWidth = 3;
        }else{
            this.roadWidth = 2;
        }
        const boundaries = [...this.boundaries];
        const top = this.boundaries[0];
        if (bernoulli1 === 1){
            const leftStep = (top.left - 1) > 1 ? (top.left - 1) : 1;
            boundaries.unshift({
                left: leftStep > 1 ? leftStep : 1,
                right: leftStep + this.roadWidth
            });
        }else{
            const leftStep = (top.left + 1 + this.roadWidth) < (this.width - 1) ? (top.left + 1) : (this.width - 1 - this.roadWidth);
            boundaries.unshift({
                left: leftStep,
                right: leftStep + this.roadWidth
            });
        }
        boundaries.pop();
        this.boundaries = boundaries;
    }

    drawRoad(){
        for (let y = 0; y < this.height; y++){
            for (let x = 0; x < this.width; x++){
                if (this.carPosition.y === y && this.carPosition.x === x){
                    this.map[y][x] = this.c.RED;
                }else{
                    if (x >= this.boundaries[y].left && x<= this.boundaries[y].right){
                        this.map[y][x] = this.c.GRAY;
                    }else{
                        this.map[y][x] = this.c.GREEN;
                    }
                }
            }
        }
        if (this.boundaries[this.carPosition.y].left > this.carPosition.x
            || this.boundaries[this.carPosition.y].right < this.carPosition.x){
            this.map[this.carPosition.y][this.carPosition.x] = this.c.BLACK;
            this.crashed = true;
        }
        return this.map;
    }

    updateCarPosition(left){
        const newX = left ? this.carPosition.x - 1 : this.carPosition.x + 1;
        this.carPosition = {x: (newX % this.width), y: 6};
    }
}


class RaceCarGame {
    constructor(matrix) {
        this.matrix = matrix;
        this.map = new Map(8, 8);

        this.start = this.start.bind(this);
    }

    async start(){
        const keyboard = new Keyboard(["left", "right"]);
        await this.matrix.setBrightness(30);
        await this.matrix.displayMatrix(this.map.drawRoad());
        keyboard.addListener(async (event) => {
            this.map.updateCarPosition(event.k === "left");
            if (!this.map.isCrashed()){
                await this.matrix.displayMatrix(this.map.drawRoad());
            }
        });
        let points = -5;
        while (!this.map.isCrashed()){
            if (points === -5){
                console.log("Mach dich bereit");
            }
            if (points === 0){
                console.log("Los !");
            }
            if (points >= 0){
                this.map.changeRoad();
            }else{
                console.log(Math.abs(points));
            }
            await this.matrix.displayMatrix(this.map.drawRoad());
            await this.matrix.wait(200);
            points++;
        }
        console.log("Crash !");
        console.log(`Points: ${points}`);
    }

}

module.exports = RaceCarGame;
