const readline = require('readline');

class Keyboard {
    constructor(filter) {
        this.eventListener = [];
        this.filter = new Set(filter);
        this.addListener = this.addListener.bind(this);

        this._start();
    }

    addListener(l){
        this.eventListener.push(l);
    }

    _start(){
        readline.emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);

        process.stdin.on('keypress', (str, key) => {
            if (key.ctrl && key.name === 'c') {
                process.exit();
            } else {
                if (this.filter.has(key.name)){
                    this.eventListener.forEach(l => l({k: key.name}));
                }
            }
        });
    }
}

module.exports = Keyboard;
