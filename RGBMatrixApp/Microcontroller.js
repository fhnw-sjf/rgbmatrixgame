const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

class Microcontroller {
    constructor(port, boundRate) {
        this.port = port;
        this.boundRate = boundRate;
        this.parser = null;
        this.listener = [];
        this.isReady = false;

        this.connect        = this.connect.bind(this);
        this.sendCommand    = this.sendCommand.bind(this);
    }

    async connect(){
        if (this.isReady) return;
        return new Promise(resolve => {
            this.serialport = new SerialPort(this.port, {
                baudRate: this.boundRate,
                autoOpen: true,
                lock: false
            });

            this.parser = this.serialport.pipe(new Readline({ delimiter: '\n' }))
            this.parser.on('data', data =>
                this.listener.forEach(l => l(JSON.parse(data.toString("utf8")))));
            this.serialport.on('error', (err) => console.log('Error: ', err.message));
            this.serialport.on('open', () => {
                this.isReady = true;
                setTimeout(() => {
                    console.log("Connected");
                    this.isReady = true;
                    resolve();
                }, 5000);
            });
        });
    }

    async sendCommand(command){
        return await new Promise(resolve => {
            if (!this.isReady) {
                resolve(false);
                return;
            }
            setTimeout(() => {
                this.serialport.write(JSON.stringify(command), (err) => {
                    if (err) {
                        console.log('Error on write: ', err.message);
                        resolve(false);
                    }else{
                        resolve(true);
                    }
                })
            }, 10);
        });
    }
}

module.exports = Microcontroller;
