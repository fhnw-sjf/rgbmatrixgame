const Microcontroller = require('./Microcontroller.js');
const RGBMatrix       = require('./RGBMatrix.js');
const RaceCarGame     = require('./games/RaceCarGame.js');

const microcontroller = new Microcontroller("COM5", 9600);
const matrix          = new RGBMatrix(microcontroller);
const game            = new RaceCarGame(matrix);

const run = async () => {
    await microcontroller.connect();
    await game.start();
}


run().then(res => console.log(res))

